package com.example.exercise14

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.exercise14.model.Model
import com.example.exercise14.network.RetrofitInstance
import kotlinx.coroutines.delay
import java.lang.Exception

class UsersPagingSource: PagingSource<Int, Model.Data>() {

    override fun getRefreshKey(state: PagingState<Int, Model.Data>): Int? {
        return null

    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Model.Data> {
        val page:Int = params.key ?: 1

        return try {
            val response = RetrofitInstance.retrofit.getData(page)
            val body = response.body()
            if (response.isSuccessful && body != null){
                var prevPage: Int? = null
                var nextPage: Int? = null
                if (page < body.totalPages)
                    nextPage = page+1
                if (page != 1)
                    prevPage = page - 1
                LoadResult.Page(
                    body.data,
                    prevPage,
                    nextPage)
            } else LoadResult.Error(Throwable())

        }catch (e: Exception){
            LoadResult.Error(e)
        }
    }
}