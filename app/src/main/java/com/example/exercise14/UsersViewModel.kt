package com.example.exercise14

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData

class UsersViewModel: ViewModel() {

    fun loadUsers() = Pager(config = PagingConfig(pageSize = 1), pagingSourceFactory = {UsersPagingSource()}).liveData.cachedIn(viewModelScope)
}