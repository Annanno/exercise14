package com.example.exercise14.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.exercise14.databinding.ItemLayoutBinding
import com.example.exercise14.extensions.setImage
import com.example.exercise14.model.Model

class UsersAdapter : PagingDataAdapter<Model.Data, UsersAdapter.ViewHolder>(
    DiffCallback()
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemLayoutBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(getItem(position)!!)
    }


    inner class ViewHolder(private val binding: ItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(model: Model.Data) {
            binding.image.setImage(model.avatar)
            binding.nameTV.text = model.firstName.plus(model.lastName)

        }

    }


    class DiffCallback : DiffUtil.ItemCallback<Model.Data>() {
        override fun areItemsTheSame(oldItem: Model.Data, newItem: Model.Data): Boolean {
           return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Model.Data, newItem: Model.Data): Boolean {
            return oldItem == newItem
        }

    }


}


