package com.example.exercise14.fragments


import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.exercise14.UsersViewModel
import com.example.exercise14.adapter.UsersAdapter
import com.example.exercise14.databinding.FragmentUsersBinding

class UsersFragment : BaseFragment<FragmentUsersBinding>(FragmentUsersBinding::inflate) {

    private val viewModel: UsersViewModel by viewModels()
    private lateinit var usersAdapter: UsersAdapter


    override fun init() {
        initRecycler()
    }

    private fun initRecycler() {
        binding.recycler.layoutManager = LinearLayoutManager(requireContext())
        usersAdapter = UsersAdapter()
        binding.recycler.adapter = usersAdapter


        usersAdapter.addLoadStateListener { loadState ->
                 binding.progressBar.isVisible = loadState.refresh is LoadState.Loading || loadState.append is LoadState.Loading
        }

        lifecycleScope.launchWhenCreated {
            viewModel.loadUsers().observe(viewLifecycleOwner, {
                usersAdapter.submitData(lifecycle, it)
            })
        }


    }


}