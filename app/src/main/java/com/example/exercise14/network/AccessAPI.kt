package com.example.exercise14.network

import com.example.exercise14.model.Model
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface AccessAPI {

    @GET("api/users")
    suspend fun getData(@Query("page") page: Int) : Response<Model>
}